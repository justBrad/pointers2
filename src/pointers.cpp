#include <iostream>

using namespace std;


int main()
{
  int x = 25, y = 2 * x;    
  auto a = &x, b = a;
  auto c = *a, d = 2 * *b;


  cout<<"x = "<<x<<", address of x = "<<&x<<endl;
  cout<<"y = "<<y<<", address of x = "<<&y<<endl;
  cout<<"a = "<<*a<<", address of x = "<<a<<endl;
  cout<<"b = "<<*b<<", address of x = "<<b<<endl;
 	cout<<"c = "<<c<<", address of x = "<<&c<<endl;
 	cout<<"d = "<<d<<", address of x = "<<&d<<endl;

}